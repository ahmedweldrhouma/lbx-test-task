<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Spatie\SimpleExcel\SimpleExcelReader;

class ImportManager implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $file;
    /**
     * Create a new job instance.
     */
    public function __construct($file)
    {
        $this->file =$file;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        //process the records by 2500
        SimpleExcelReader::create($this->file)
            ->useDelimiter(',')
            ->skip(1)
            ->getRows()
            ->chunk(2500)
            ->each(
                fn ($chunk) => ProcessCsvImport::dispatch($chunk)
            );
    }
}
