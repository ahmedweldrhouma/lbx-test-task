## set up application

Copy the .env.example to .env file

## Install dependencies
install php dependencies
```
composer install
```
generate app key
```
php artisan key:generate
```

## set up database

set your database configuration in the .env file modify the :
DB_HOST
DB_USERNAME
DB_PASSWORD
DB_DATABASE

## set table jobs
```
php artisan make:queue-table
```
## migrate the DB schema

```
php artisan migrate
```

## run the app

```
php artisan serve
```
## 1 : run this command to listen to the queues

```
php artisan queue:listen 
```

## 2 : upload the import file and watch to queues working in background 


