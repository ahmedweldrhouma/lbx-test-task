<?php

namespace App\Http\Controllers;

use App\Jobs\ImportManager;
use App\Repositories\EmployeeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class EmployeeController extends Controller
{
    private EmployeeRepository $employeeRepository;

    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response()->json($this->employeeRepository->paginate(50));
    }
    /**
     * Store a list of employees.
     */
    public function store(Request $request)
    {
        $content = $request->getContent();
        // Check if a file is present in the request
        if ($content != "") {
            Storage::disk('public')->put('import.csv', $request->getContent());
            // Dispatch job to process the CSV file in the background
            ImportManager::dispatch(Storage::path('public/import.csv'));
            return response()->json(['message' => 'CSV import process has been initiated'], 200);
        } else {
            return response()->json(['error' => 'No file uploaded'], 400);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return Response()->json($this->employeeRepository->find($id));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if ($this->employeeRepository->delete($id)){
            return 'success';
        }
        return 'failed';
    }
}
