<?php

namespace App\Repositories;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;

class EmployeeRepository extends BaseRepository
{
    public function model(): string
    {
        return Employee::class;
    }

    public function add($data)
    {
        try {
            return $this->create([
                'employee_id' => $data['employee_id']?? null,
                'user_name' => $data['user_name']?? null,
                'name_prefix' => $data['name_prefix']?? null,
                'first_name' => $data['first_name']?? null,
                'middle_initial' => $data['middle_initial']?? null,
                'last_name' => $data['last_name']?? null,
                'gender' => $data['gender']?? null,
                'email' => $data['email']?? null,
                'birth_date' => $data['birth_date']?? null,
                'birth_time' => $data['birth_time']?? null,
                'age' => $data['age']?? null,
                'joined_at' => $data['joined_at']?? null,
                'age_in_company' => $data['age_in_company']?? null,
                'phone' => $data['phone']?? null,
                'place_name' => $data['place_name']?? null,
                'county' => $data['country']?? null,
                'city' => $data['city']?? null,
                'zip' => $data['zip']?? null,
                'region' => $data['region']?? null,
            ]);
        }catch (\Exception $e){
            return false;
        }
    }


    public function updateEmployee(Employee $employee,array $data)
    {
        try {
            return $this->update([
                'employee_id' => $data['employee_id']??  $employee->employee_id,
                'user_name' => $data['user_name']??  $employee->user_name,
                'name_prefix' => $data['name_prefix']??  $employee->name_prefix,
                'first_name' => $data['first_name']??  $employee->first_name,
                'middle_initial' => $data['middle_initial']??  $employee->middle_initial,
                'last_name' => $data['last_name']??  $employee->last_name,
                'gender' => $data['gender']??  $employee->gender,
                'email' => $data['email']??  $employee->email,
                'birth_date' => $data['birth_date']??  $employee->birth_date,
                'birth_time' => $data['birth_time']??  $employee->birth_time,
                'age' => $data['age']??  $employee->age,
                'joined_at' => $data['joined_at']??  $employee->joined_at,
                'age_in_company' => $data['age_in_company']??  $employee->age_in_company,
                'phone' => $data['phone']??  $employee->phone,
                'place_name' => $data['place_name']??  $employee->place_name,
                'county' => $data['country']??  $employee->country,
                'city' => $data['city']??  $employee->city,
                'zip' => $data['zip']??  $employee->zip,
                'region' => $data['region']??  $employee->region,
            ],$employee->id);
        }catch (\Exception $e){
            return false;
        }
    }
    public function updateprioritys(array $data)
    {
        DB::beginTransaction();
        try {
            foreach ($data['employees'] as $i => $employee){
                $this->update([
                    'priority' => $i,
                ],$employee);
            }

        }catch (\Exception $e){
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }

    public function remove(Employee $employee)
    {
        try {
            return $this->delete($employee->id);
        }catch (\Exception $e){
            return false;
        }
    }

    public function getFieldsSearchable(): array
    {
        return [
            'id'
        ];
    }
}
