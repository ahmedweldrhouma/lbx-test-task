<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'employee_id' => ['string', 'required'],
            'user_name' => ['string', 'required'],
            'name_prefix' => ['string', 'required'],
            'first_name' => ['string', 'required'],
            'middle_initial' => ['string', 'required'],
            'last_name' => ['string', 'required'],
            'gender' => ['string', 'required'],
            'email' => ['string', 'required'],
            'birth_date' => ['string', 'required'],
            'birth_time' => ['string', 'required'],
            'age' => ['string', 'required'],
            'joined_at' => ['string', 'required'],
            'age_in_company' => ['string', 'required'],
            'phone' => ['string', 'required'],
            'place_name' => ['string', 'required'],
            'county' => ['string', 'required'],
            'city' => ['string', 'required'],
            'zip' => ['string', 'required'],
            'region' => ['string', 'required']
        ];
    }

}
