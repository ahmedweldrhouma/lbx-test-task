<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
protected $primaryKey = 'employee_id';
    public $timestamps = false;
    protected $fillable = [
        'employee_id',
        'name_prefix',
        'first_name',
        'middle_initial',
        'last_name',
        'gender',
        'email',
        'birth_date',
        'birth_time',
        'age',
        'joined_at',
        'age_in_company',
        'phone',
        'place_name',
        'county',
        'city',
        'zip',
        'region',
        'user_name'
    ];

    public const SEARCHABLE = [
        'employee_id',
    ];
    public const COLUMNS = [
        'employee_id',
        'name_prefix',
        'first_name',
        'middle_initial',
        'last_name',
        'gender',
        'email',
        'birth_date',
        'birth_time',
        'age',
        'joined_at',
        'age_in_company',
        'phone',
        'place_name',
        'county',
        'city',
        'zip',
        'region',
        'user_name'
    ];
    public const RULES = [
        'employee_id' => ['int', 'required'],
        'user_name' => ['string', 'required'],
        'name_prefix' => ['string', 'required'],
        'first_name' => ['string', 'required'],
        'middle_initial' => ['string', 'required'],
        'last_name' => ['string', 'required'],
        'gender' => ['string', 'required'],
        'email' => ['string', 'required'],
        'birth_date' => ['date', 'required'],
        'birth_time' => ['required'],
        'age' => ['numeric', 'required'],
        'joined_at' => ['date', 'required'],
        'age_in_company' => ['numeric', 'required'],
        'phone' => ['string', 'required'],
        'place_name' => ['string', 'required'],
        'county' => ['string', 'required'],
        'city' => ['string', 'required'],
        'zip' => ['string', 'required'],
        'region' => ['string', 'required']
    ];
}
