<?php

namespace App\Jobs;

use App\Http\Requests\StoreEmployee;
use App\Models\Employee;
use App\Rules\TimeRule;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Validator;

class ProcessCsvImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $chunk;

    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @return void
     */
    public function __construct($chunk)
    {
        $this->chunk = $chunk;
    }


    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $insert = [];
        // Process the records
        foreach ($this->chunk as $record) {
            $record = array_combine(Employee::COLUMNS, $record);
            $validator = Validator::make($record, Employee::RULES);
            //check row data by validation and save only the validated data
            if (!$validator->fails() && $this->validateTime($record)) {
                $this->reform($record);
                $insert[] = $record;
            }
        }
        // Process the chunk rows and insert into database
        Employee::insert($insert);
    }

    private function reform(&$record)
    {
        $record['birth_date'] = Carbon::make($record['birth_date'])->format('Y-m-d');
        $record['joined_at'] = Carbon::make($record['joined_at'])->format('Y-m-d');
        $record['birth_time'] = Carbon::make($record['birth_time'])->format('H:i:s');
    }

    private function validateTime($record)
    {
        [$time,$timezone] = explode(' ' ,$record['birth_time']);
        return in_array($timezone,['AM','PM']) && strlen($time) < 9;
    }
}
